﻿using Our.Umbraco.Cdn.Management.Models;

namespace Our.Umbraco.Cdn.Management.Akamai
{
    public interface IAkamaiCdnConfig
    {
        string? ClientSecret { get; }
        string? Host { get; }
        string? AccessToken { get; }
        string? ClientToken { get; }
        string? Network { get; }
    }

    public class AkamaiCdnConfig : IAkamaiCdnConfig
    {
        public const string ServiceAlias = "Akamai";
        public const string ClientSecretAlias = "clientSecret";
        public const string HostAlias = "host";
        public const string AccessTokenAlias = "accessToken";
        public const string ClientTokenAlias = "clientToken";
        public const string NetworkAlias = "network";

        private readonly ICdnConfiguration _cdnConfiguration;

        public AkamaiCdnConfig(ICdnConfiguration cdnConfiguration)
        {
            _cdnConfiguration = cdnConfiguration;
        }

        public string? ClientSecret => _cdnConfiguration.GetApiProperty(ClientSecretAlias, ServiceAlias);
        public string? Host => _cdnConfiguration.GetApiProperty(HostAlias, ServiceAlias);
        public string? AccessToken => _cdnConfiguration.GetApiProperty(AccessTokenAlias, ServiceAlias);
        public string? ClientToken => _cdnConfiguration.GetApiProperty(ClientTokenAlias, ServiceAlias);
        public string? Network => _cdnConfiguration.GetApiProperty(NetworkAlias, ServiceAlias);
    }
}
