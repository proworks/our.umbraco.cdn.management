﻿using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.Akamai
{
    public static class ServiceCollectionExtensions
    {
        public static IUmbracoBuilder ConfigureAkamaiCdnManagement(this IUmbracoBuilder builder, string writableConfigFile = "appsettings.json")
            => builder
                .ConfigureCdnManagement(writableConfigFile)
                .AddServices();

        private static IUmbracoBuilder AddServices(this IUmbracoBuilder builder)
        {
            builder.Services.AddUnique<ICdnApiService, AkamaiCdnApiService>();
            builder.Services.AddUnique<IAkamaiCdnConfig, AkamaiCdnConfig>();

            return builder;
        }
    }
}
