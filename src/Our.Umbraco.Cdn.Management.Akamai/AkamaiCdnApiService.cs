﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.Services;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;

namespace Our.Umbraco.Cdn.Management.Akamai
{
    public class AkamaiCdnApiService : ICdnApiService
    {
        private const string AuthorizationScheme = "EG1-HMAC-SHA256";
        private const string ClientTokenName = "client_token";
        private const string AccessTokenName = "access_token";
        private const string TimestampName = "timestamp";
        private const string NonceName = "nonce";
        private const string SignatureName = "signature";
        private const int MaxContentSize = 50000;

        private static readonly Encoding _encoding = new UTF8Encoding(false);

        private readonly ILogger<AkamaiCdnApiService> _logger;
        private readonly IAkamaiCdnConfig _config;

        public AkamaiCdnApiService(ILogger<AkamaiCdnApiService> logger, IAkamaiCdnConfig config)
        {
            _logger = logger;
            _config = config;
        }

        public bool CanPurgeSite { get; } = false;
        public string ServiceAlias { get; } = AkamaiCdnConfig.ServiceAlias;
        public string ServiceName { get; } = "Akamai";
        public IEnumerable<CdnApiPropertyDefinition> Properties { get; } = new[]
        {
            new CdnApiPropertyDefinition(AkamaiCdnConfig.ClientSecretAlias, "Client Secret", "The client_secret value from the credential"),
            new CdnApiPropertyDefinition(AkamaiCdnConfig.HostAlias, "Host", "The host value from the credential"),
            new CdnApiPropertyDefinition(AkamaiCdnConfig.AccessTokenAlias, "Access Token", "The access_token value from the credential"),
            new CdnApiPropertyDefinition(AkamaiCdnConfig.ClientTokenAlias, "Client Token", "The client_token value from the credential"),
            new CdnApiPropertyDefinition(AkamaiCdnConfig.NetworkAlias, "Network", "The network on which you want to invalidate or delete content, either 'staging' or 'production'.  If not specified, production is used.", false)
        };
        public string ServiceOverview { get; } = "<p>See <a href='https://developer.akamai.com/legacy/introduction/Prov_Creds.html' target='_blank'>https://developer.akamai.com/legacy/introduction/Prov_Creds.html</a> for instructions on how to authorize a client.  That process will provide you with a credential containing 4 values, which will be used in the fields below.</p>";

        public Task<CdnResponse> PurgeSite()
        {
            throw new NotImplementedException();
        }

        public async Task<CdnResponse> PurgeUrls(IEnumerable<string?>? urls)
        {
            if (urls?.ToList() is not List<string> urlList || urlList.Count == 0) return "";
            if (!ValidateConfig(out var response)) return response;

            var content = JsonConvert.SerializeObject(new PurgeUrlsRequest { Objects = urlList }, Formatting.None);

            if (_encoding.GetByteCount(content) <= MaxContentSize) return await PurgeContent(content).ConfigureAwait(false);

            var skip = 0;
            var purge = new PurgeUrlsRequest();

            while (skip < urlList.Count)
            {
                var take = 50 > (urlList.Count - skip) ? urlList.Count - skip : 50;
                purge.Objects = urlList.Skip(skip).Take(take);

                // If we are already beyond the limit, reduce the amount we can take
                while (take > 0 && _encoding.GetByteCount(JsonConvert.SerializeObject(purge, Formatting.None)) > MaxContentSize) purge.Objects = urlList.Skip(skip).Take(--take);

                if (take == 0)
                {
                    _logger.LogError("The URL is too long to process with a maximum request size of {max}: {url}", MaxContentSize, urlList[skip]);
                    return $"One or more URLs are too long to process.  The maximum request size is {MaxContentSize} bytes";
                }

                if (take == 50)
                {
                    // Find the first that goes longer than the limit (or is beyond the max count), and then go 1 less than that
                    while (take < (urlList.Count - skip) && _encoding.GetByteCount(JsonConvert.SerializeObject(purge, Formatting.None)) <= MaxContentSize) purge.Objects = urlList.Skip(skip).Take(++take);
                    purge.Objects = urlList.Skip(skip).Take(--take);
                }

                content = JsonConvert.SerializeObject(purge, Formatting.None);
                var result = await PurgeContent(content).ConfigureAwait(false);
                if (!string.IsNullOrWhiteSpace(result.Error)) return result;

                skip += take;
            }

            return "";
        }

        public Task<CdnResponse> ValidateConnection(Uri siteUrl) => PurgeUrls(new[] { new Uri(siteUrl, "robots.txt").ToString() });

        private bool ValidateConfig(out CdnResponse response)
        {
            var errors = new List<string>();

            if (_config.ClientSecret == null) errors.Add("Missing the Client Secret.");
            if (_config.Host == null) errors.Add("Missing the Host.");
            if (_config.AccessToken == null) errors.Add("Missing the Access Token.");
            if (_config.ClientToken == null) errors.Add("Missing the Client Token.");

            response = errors.Count == 0 ? "" : string.Join("  ", errors);
            return errors.Count == 0;
        }

        private async Task<CdnResponse> PurgeContent(string content)
        {
            if (_config.ClientSecret == null) return "No client secret specified in the config";

            var relativeUrl = "/ccu/v3/invalidate/url";
            if (!string.IsNullOrWhiteSpace(_config.Network)) relativeUrl += $"/{_config.Network}";

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, new Uri($"https://{_config.Host}{relativeUrl}")))
            using (var httpContent = new StringContent(content, _encoding, "application/json"))
            {
                request.Headers.ExpectContinue = false;
                request.Headers.Authorization = new AuthenticationHeaderValue(AuthorizationScheme, GetAuthorizationHeader(relativeUrl, content, _config.ClientSecret));
                request.Content = httpContent;

                using (var response = await client.SendAsync(request).ConfigureAwait(false))
                {
                    var resultContent = response.Content == null ? null : await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var result = !string.IsNullOrWhiteSpace(resultContent) && resultContent[0] == '{' ? JsonConvert.DeserializeObject<PurgeResponse>(resultContent) : null;
                    if (!response.IsSuccessStatusCode || result == null)
                    {
                        _logger.LogError("Akamai could not handle purge request.  StatusCode={code}, Request={request}, Response={response}", response.StatusCode, content, resultContent);
                        return $"Akamai returned a response code of {response.StatusCode}";
                    }

                    if (result.HttpStatus >= 200 && result.HttpStatus < 300) return "";

                    _logger.LogError("Akamai could not process purge request.  StatusCode={code}, Request={request}, Response={response}", response.StatusCode, content, resultContent);
                    return $"Akamai returned a {result.HttpStatus}-{result.Title} error: {result.Detail}";
                }
            }
        }

        private string GetAuthorizationHeader(string relativeUrl, string content, string clientSecret)
        {
            // From https://developer.akamai.com/legacy/introduction/Identity_Model.html and https://developer.akamai.com/legacy/introduction/Client_Auth.html
            var timestamp = DateTime.UtcNow.ToString("yyyyMMdd'T'HH:mm:ss") + "+0000";
            var signingKey = Sign(clientSecret, timestamp);
            var authPrefix = $"{ClientTokenName}={_config.ClientToken};{AccessTokenName}={_config.AccessToken};{TimestampName}={timestamp};{NonceName}={Guid.NewGuid()};";
            string contentHash;

            using (var hash = SHA256.Create()) contentHash = Convert.ToBase64String(hash.ComputeHash(_encoding.GetBytes(content)));

            var dataToSign = $"POST\thttps\t{_config.Host?.ToLowerInvariant()}\t{relativeUrl}\t\t{contentHash}\t{AuthorizationScheme} {authPrefix}";
            var signature = Sign(signingKey, dataToSign);
            var authHeader = $"{authPrefix}{SignatureName}={signature}";

            return authHeader;
        }

        private static string Sign(string key, string content)
        {
            using var hash = new HMACSHA256(_encoding.GetBytes(key));
            return Convert.ToBase64String(hash.ComputeHash(_encoding.GetBytes(content)));
        }

        private class PurgeUrlsRequest
        {
            [JsonProperty("objects")]
            public IEnumerable<string?>? Objects { get; set; }
        }

        private class PurgeResponse
        {
            [JsonProperty("describedBy")]
            public string? DescribedBy { get; set; }

            [JsonProperty("detail")]
            public string? Detail { get; set; }

            [JsonProperty("estimatedSeconds")]
            public int EstimatedSeconds { get; set; }

            [JsonProperty("httpStatus")]
            public int HttpStatus { get; set; }

            [JsonProperty("purgeId")]
            public string? PurgeId { get; set; }

            [JsonProperty("supportId")]
            public string? SupportId { get; set; }

            [JsonProperty("title")]
            public string? Title { get; set; }
        }
    }
}
