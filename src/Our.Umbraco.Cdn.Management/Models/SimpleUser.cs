﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class SimpleUser
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("key")]
        public Guid Key { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("email")]
        public string? Email { get; set; }
    }
}
