﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class AutoPurgeConfiguration
    {
        [JsonProperty("handlers")]
        public HandlerConfig?[]? Handlers { get; set; }
    }

    public class HandlerConfig
    {
        [JsonProperty("alias")]
        public string? Alias { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
    }
}
