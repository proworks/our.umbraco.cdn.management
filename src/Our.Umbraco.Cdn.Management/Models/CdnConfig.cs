﻿namespace Our.Umbraco.Cdn.Management.Models
{
    public class CdnConfig
    {
        public ICollection<ApiConfig>? ApiConfigs { get; set; }
        public ICollection<AutoPurgeConfig>? AutoPurgeConfigs { get; set; }
        public ICollection<string>? PurgeMenuGroups { get; set; }

        public class ApiConfig
        {
            public string? ServiceAlias { get; set; }
            public Dictionary<string, string>? Config { get; set; }
        }

        public class AutoPurgeConfig
        {
            public string? Handler { get; set; }
            public bool AutoPurge { get; set; }
        }
    }
}
