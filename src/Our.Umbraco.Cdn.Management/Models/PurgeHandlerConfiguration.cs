﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class PurgeHandlerConfiguration
    {
        [JsonProperty("alias")]
        public string? Alias { get; set; }

        [JsonProperty("label")]
        public string? Label { get; set; }

        [JsonProperty("view")]
        public string? View { get; set; }

        [JsonProperty("config")]
        public object? Config { get; set; }
    }
}
