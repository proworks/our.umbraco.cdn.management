﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class CdnApiPropertyDefinition
    {
        public CdnApiPropertyDefinition(string alias, string name, string? description = null, bool required = true, string? defaultValue = null)
        {
            Alias = alias;
            Name = name;
            Description = description;
            Required = required;
            DefaultValue = defaultValue;
        }

        [JsonProperty("alias")]
        public string Alias { get; }

        [JsonProperty("name")]
        public string Name { get; }

        [JsonProperty("description")]
        public string? Description { get; }

        [JsonProperty("required")]
        public bool Required { get; }

        [JsonProperty("defaultValue")]
        public string? DefaultValue { get; }
    }
}
