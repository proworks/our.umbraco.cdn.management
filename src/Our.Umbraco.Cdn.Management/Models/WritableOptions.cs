﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Our.Umbraco.Cdn.Management.Models
{
    public interface IWritableOptions<out T> : IOptionsMonitor<T> where T : class, new()
    {
        void Update(Action<T> applyChanges);
        Task UpdateAsync(Action<T> applyChanges);
        Task UpdateAsync(Func<T, Task> applyChanges);
    }

    public class WritableOptions<T> : IWritableOptions<T> where T : class, new()
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IOptionsMonitor<T> _options;
        private readonly IConfigurationRoot _configuration;
        private readonly string _section;
        private readonly string _file;

        public WritableOptions(
            IWebHostEnvironment environment,
            IOptionsMonitor<T> options,
            IConfigurationRoot configuration,
            string section,
            string file)
        {
            _environment = environment;
            _options = options;
            _configuration = configuration;
            _section = section;
            _file = file;
        }

        public T CurrentValue => _options.CurrentValue;
        public T Get(string name) => _options.Get(name);
        public IDisposable OnChange(Action<T, string> listener) => _options.OnChange(listener);

        public void Update(Action<T> applyChanges)
        {
            var fileProvider = _environment.ContentRootFileProvider;
            var fileInfo = fileProvider.GetFileInfo(_file);
            var physicalPath = fileInfo.PhysicalPath;

            var jObject = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(physicalPath)) ?? new JObject();
            var sectionObject = (jObject.TryGetValue(_section, out var section) ?
                JsonConvert.DeserializeObject<T>(section.ToString())
                : CurrentValue)
                ?? new T();

            applyChanges?.Invoke(sectionObject);

            jObject[_section] = JObject.Parse(JsonConvert.SerializeObject(sectionObject));
            File.WriteAllText(physicalPath, JsonConvert.SerializeObject(jObject, Formatting.Indented));
            _configuration.Reload();
        }

        public Task UpdateAsync(Action<T> applyChanges) => UpdateAsync(x => { applyChanges(x); return Task.CompletedTask; });

        public async Task UpdateAsync(Func<T, Task> applyChanges)
        {
            var fileProvider = _environment.ContentRootFileProvider;
            var fileInfo = fileProvider.GetFileInfo(_file);
            var physicalPath = fileInfo.PhysicalPath;
            string contents;

            using (var file = File.OpenText(physicalPath))
            {
                contents = await file.ReadToEndAsync().ConfigureAwait(false);
            }

            var jObject = JsonConvert.DeserializeObject<JObject>(contents) ?? new JObject();
            var sectionObject = (jObject.TryGetValue(_section, out var section) ?
                JsonConvert.DeserializeObject<T>(section.ToString())
                : CurrentValue)
                ?? new T();

            await applyChanges(sectionObject).ConfigureAwait(false);

            jObject[_section] = JObject.Parse(JsonConvert.SerializeObject(sectionObject));
            contents = JsonConvert.SerializeObject(jObject, Formatting.Indented);

            using (var file = File.OpenWrite(physicalPath))
            using (var writer = new StreamWriter(file))
            {
                await writer.WriteAsync(contents).ConfigureAwait(false);
                await writer.FlushAsync().ConfigureAwait(false);
            }

            _configuration.Reload();
        }
    }
}
