﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class LastCdnResponse
    {
        [JsonProperty("response")]
        public CdnResponse? Response { get; set; }

        [JsonProperty("resource")]
        public string? Resource { get; set; }

        [JsonProperty("user")]
        public SimpleUser? User { get; set; }

        [JsonProperty("dateTime")]
        public string? DateTime { get; set; }
    }
}
