﻿using Our.Umbraco.Cdn.Management.PurgeHandlers;
using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core.Security;
using static Our.Umbraco.Cdn.Management.Models.CdnConfig;

namespace Our.Umbraco.Cdn.Management.Models
{
    public interface ICdnConfiguration
    {
        IEnumerable<string> AllowedMenuPurgeGroups { get; }

        bool ShouldAutoPurge(string handler);
        bool ShouldShowPurgeMenu();
        IReadOnlyDictionary<string, string> GetApiConfig(string? apiServiceAlias = null);
        string? GetApiProperty(string propertyAlias, string? apiServiceAlias = null);
    }

    public interface ICdnConfigurationWriter
    {
        Task UpdateAccount(AccountConfiguration updated);
        Task UpdateAutoPurge(string handler, bool enabled);
        Task UpdateAllowedMenuPurgeGroups(IEnumerable<string> allowed);
    }

    public class CdnConfiguration : ICdnConfiguration, ICdnConfigurationWriter
    {
        private readonly IBackOfficeSecurityAccessor _backOfficeSecurity;
        private readonly Lazy<IEnumerable<ICdnPurgeHandler>> _cdnPurgeHandlers;
        private readonly Lazy<ICdnApiService> _cdnApiService;
        private readonly IWritableOptions<CdnConfig> _config;

        public CdnConfiguration(IBackOfficeSecurityAccessor backOfficeSecurity, Lazy<IEnumerable<ICdnPurgeHandler>> cdnPurgeHandlers, Lazy<ICdnApiService> cdnApiService, IWritableOptions<CdnConfig> config)
        {
            _backOfficeSecurity = backOfficeSecurity;
            _cdnPurgeHandlers = cdnPurgeHandlers;
            _cdnApiService = cdnApiService;
            _config = config;
        }

        public IReadOnlyDictionary<string, IReadOnlyDictionary<string, string>> ApiConfig
            => _config.CurrentValue.ApiConfigs?.Where(x => x.ServiceAlias != null)
                .ToDictionary(p => p.ServiceAlias ?? string.Empty, p => (IReadOnlyDictionary<string, string>)(p.Config ?? new Dictionary<string, string>()))
            ?? new Dictionary<string, IReadOnlyDictionary<string, string>>();

        public IEnumerable<string> AllowedMenuPurgeGroups
            => _config.CurrentValue.PurgeMenuGroups ?? Enumerable.Empty<string>();

        public IReadOnlyDictionary<string, string> GetApiConfig(string? apiServiceAlias = null)
            => _config.CurrentValue.ApiConfigs?.FirstOrDefault(x => x.ServiceAlias == (apiServiceAlias ?? _cdnApiService.Value.ServiceAlias))?.Config ?? new Dictionary<string, string>();

        public string? GetApiProperty(string propertyAlias, string? apiServiceAlias = null)
            => GetApiConfig(apiServiceAlias).TryGetValue(propertyAlias, out var value) && value != null ? value : null;

        public bool ShouldAutoPurge(string handler)
            => _config.CurrentValue.AutoPurgeConfigs?.FirstOrDefault(x => x.Handler == handler)?.AutoPurge ?? false;

        public bool ShouldShowPurgeMenu()
        {
            var groups = _backOfficeSecurity.BackOfficeSecurity?.CurrentUser?.Groups?.Select(g => g.Alias).ToList();

            if (groups == null || groups.Count == 0) return false;

            var grps = _config.CurrentValue.PurgeMenuGroups;
            return grps != null && grps.Any(g => groups.Contains(g, StringComparer.InvariantCultureIgnoreCase));
        }

        public async Task UpdateAccount(AccountConfiguration updated)
        {
            var propVals = new Dictionary<string, string>(updated.PropertyValues?.OfType<KeyValuePair<string, string>>().ToDictionary(x => x.Key, x => x.Value) ?? new Dictionary<string, string>());

            foreach(var prop in _cdnApiService.Value.Properties)
            {
                if (!prop.Required || (propVals.TryGetValue(prop.Alias, out var value) && !string.IsNullOrWhiteSpace(value))) continue;

                throw new ArgumentNullException(prop.Name);
            }

            await _config.UpdateAsync(x =>
            {
                if (x.ApiConfigs?.FirstOrDefault(y => y.ServiceAlias == updated.ServiceAlias) is ApiConfig cfg)
                {
                    cfg.Config = propVals;
                }
                else if (updated.ServiceAlias != null)
                {
                    if (x.ApiConfigs == null) x.ApiConfigs = new List<ApiConfig>();
                    x.ApiConfigs.Add(new() { ServiceAlias = updated.ServiceAlias, Config = propVals });
                }
            }).ConfigureAwait(false);
        }

        public async Task UpdateAllowedMenuPurgeGroups(IEnumerable<string> allowed)
        {
            await _config.UpdateAsync(x =>
            {
                if (x.PurgeMenuGroups == null) x.PurgeMenuGroups = new List<string>(allowed);
                else
                {
                    x.PurgeMenuGroups.Clear();

                    foreach (var allow in allowed) x.PurgeMenuGroups.Add(allow);
                }
            }).ConfigureAwait(false);
        }

        public async Task UpdateAutoPurge(string handler, bool enabled)
        {
            var purger = _cdnPurgeHandlers.Value.FirstOrDefault(h => h.Alias == handler);
            if (purger == null || !purger.CanSetAutoPurge) return;

            if (enabled) purger.StartAutoPurge();
            else purger.StopAutoPurge();

            await _config.UpdateAsync(x =>
            {
                var config = x.AutoPurgeConfigs?.FirstOrDefault(x => x.Handler == handler);
                if (config == null && enabled)
                {
                    if (x.AutoPurgeConfigs == null) x.AutoPurgeConfigs = new List<AutoPurgeConfig>();
                    x.AutoPurgeConfigs.Add(new() { Handler = handler, AutoPurge = enabled });
                }
                else if (config != null) config.AutoPurge = enabled;
            }).ConfigureAwait(false);
        }
    }
}
