﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class AccountConfiguration
    {
        [JsonProperty("serviceAlias")]
        public string? ServiceAlias { get; set; }

        [JsonProperty("serviceName")]
        public string? ServiceName { get; set; }

        [JsonProperty("serviceOverview")]
        public string? ServiceOveriew { get; set; }

        [JsonProperty("propertyValues")]
        public Dictionary<string, string?>? PropertyValues { get; set; }

        [JsonProperty("propertyDefinitions")]
        public IEnumerable<CdnApiPropertyDefinition?>? PropertyDefinitions { get; set; }
    }
}
