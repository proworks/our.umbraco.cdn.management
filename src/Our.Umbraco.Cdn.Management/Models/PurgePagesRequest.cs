﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class PurgePagesRequest
    {
        [JsonProperty("includeChildren")]
        public bool IncludeChildren { get; set; }

        [JsonProperty("pagesByHandler")]
        public IEnumerable<PurgePageHandlerRequest?>? PagesByHandler { get; set; }
    }

    public class PurgePageHandlerRequest
    {
        [JsonProperty("handlerAlias")]
        public string? HandlerAlias { get; set; }

        [JsonProperty("pages")]
        public IEnumerable<string?>? Pages { get; set; }
    }
}
