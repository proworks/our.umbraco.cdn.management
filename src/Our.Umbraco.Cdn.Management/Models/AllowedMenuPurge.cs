﻿using Newtonsoft.Json;
using Umbraco.Cms.Core.Models.ContentEditing;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class AllowedMenuPurge
    {
        [JsonProperty("groups")]
        public IEnumerable<UserGroupBasic?>? Groups { get; set; }
    }
}
