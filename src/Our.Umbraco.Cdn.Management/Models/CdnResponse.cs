﻿using Newtonsoft.Json;

namespace Our.Umbraco.Cdn.Management.Models
{
    public class CdnResponse
    {
        [JsonProperty("error")]
        public string? Error { get; set; }

        [JsonProperty("message")]
        public string? Message { get; set; }

        public static implicit operator CdnResponse(string? value) => new() { Error = string.IsNullOrWhiteSpace(value) ? null : value };
    }
}
