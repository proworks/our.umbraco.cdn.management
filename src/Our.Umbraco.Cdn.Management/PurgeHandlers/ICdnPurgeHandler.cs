﻿namespace Our.Umbraco.Cdn.Management.PurgeHandlers
{
    public interface ICdnPurgeHandler
    {
        string Alias { get; }
        string Name { get; }
        string? PickerView { get; }
        object? PickerConfig { get; }
        bool AutoPurge { get; }
        bool CanSetAutoPurge { get; }

        IEnumerable<string> GetAllPageUrls();
        IEnumerable<string> GetPageUrls(ILookup<string, string> pagesByHandler, bool includeChildren);

        void Initialize();
        void Terminate();
        void StartAutoPurge();
        void StopAutoPurge();
    }
}
