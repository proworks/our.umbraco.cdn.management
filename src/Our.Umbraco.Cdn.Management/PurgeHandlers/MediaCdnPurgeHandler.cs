﻿using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Media;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.PropertyEditors.ValueConverters;
using Umbraco.Cms.Core.Scoping;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.PurgeHandlers
{
    public class MediaCdnPurgeHandler : ICdnPurgeHandler
    {
        public const string MediaAlias = "media";
        private readonly IUmbracoContextFactory _umbracoContextFactory;
        private readonly IScopeProvider _scopeProvider;
        private readonly IImageUrlGenerator _imageUrlGenerator;
        private readonly MenuRenderingHandler _menuRenderingHandler;
        private readonly MediaSavedHandler _mediaSavedHandler;
        private readonly MediaDeletingHandler _mediaDeletingHandler;

        public MediaCdnPurgeHandler(IUmbracoContextFactory umbracoContextFactory,
                                    IScopeProvider scopeProvider,
                                    IImageUrlGenerator imageUrlGenerator,
                                    MenuRenderingHandler menuRenderingHandler,
                                    MediaSavedHandler mediaSavedHandler,
                                    MediaDeletingHandler mediaDeletingHandler)
        {
            _umbracoContextFactory = umbracoContextFactory;
            _scopeProvider = scopeProvider;
            _imageUrlGenerator = imageUrlGenerator;
            _menuRenderingHandler = menuRenderingHandler;
            _mediaSavedHandler = mediaSavedHandler;
            _mediaDeletingHandler = mediaDeletingHandler;
        }

        public string Alias { get; } = MediaAlias;
        public string Name { get; } = "Media";
        public string PickerView { get; } = "mediapicker";
        public object PickerConfig { get; } = new Dictionary<string, object> { ["multiPicker"] = "1" };
        public bool CanSetAutoPurge { get; } = true;
        public bool AutoPurge { get; private set; }

        public IEnumerable<string> GetAllPageUrls()
        {
            using (var scope = _scopeProvider.CreateScope())
            using (var ucr = _umbracoContextFactory.EnsureUmbracoContext())
            {
                try
                {
                    var uc = ucr.UmbracoContext;
                    var urls = GetUrls(uc.Media.GetAtRoot(), true);

                    return urls;
                }
                finally
                {
                    scope.Complete();
                }
            }
        }

        public IEnumerable<string> GetPageUrls(ILookup<string, string> pagesByHandler, bool includeChildren)
        {
            using (var scope = _scopeProvider.CreateScope())
            using (var ucr = _umbracoContextFactory.EnsureUmbracoContext())
            {
                try
                {
                    var uc = ucr.UmbracoContext;
                    var urls = GetUrls(pagesByHandler[MediaAlias].Select(p =>
                        UdiParser.TryParse(p, out GuidUdi udi) && udi.EntityType == Constants.UdiEntityType.Media
                        ? uc.Media.GetById(udi.Guid)
                        : (Guid.TryParse(p, out var guid) ? uc.Media.GetById(guid) : (int.TryParse(p, out var id) ? uc.Media.GetById(id) : null))
                    ), includeChildren);

                    return urls;
                }
                finally
                {
                    scope.Complete();
                }
            }
        }

        public void Initialize() { }
        public void Terminate() { }

        private IEnumerable<string> GetUrls(IEnumerable<IPublishedContent?>? contents, bool includeChildren)
        {
            if (contents == null) yield break;

            foreach (var content in contents)
            {
                if (content == null) continue;

                foreach (var url in GetUrls(content)) yield return url;

                if (includeChildren)
                {
                    foreach (var child in content.Descendants())
                    {
                        foreach (var url in GetUrls(child)) yield return url;
                    }
                }
            }
        }

        private IEnumerable<string> GetUrls(IPublishedContent content)
        {
            if (content == null) yield break;

            var url = content.Url(mode: UrlMode.Absolute);
            if (!string.IsNullOrWhiteSpace(url))
            {
                yield return url;

                var uri = new Uri(url, UriKind.Absolute);
                var icv = content.Value<ImageCropperValue>(Constants.Conventions.Media.File);
                if (icv?.Crops != null)
                {
                    foreach (var crop in icv.Crops)
                    {
                        url = icv.GetCropUrl(crop.Alias, _imageUrlGenerator);
                        if (Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out var cropUri))
                            yield return cropUri.IsAbsoluteUri ? cropUri.ToString() : new Uri(uri, cropUri).ToString();
                    }
                }
            }
        }

        public void StartAutoPurge()
        {
            if (AutoPurge) return;

            _mediaSavedHandler.Enabled = true;
            _mediaDeletingHandler.Enabled = true;

            AutoPurge = true;
        }

        public void StopAutoPurge()
        {
            if (!AutoPurge) return;

            _mediaSavedHandler.Enabled = false;
            _mediaDeletingHandler.Enabled = false;

            AutoPurge = false;
        }

        public abstract class BaseContentHandler
        {
            protected readonly Lazy<ICdnManagementService> _cdnManagementService;

            public BaseContentHandler(Lazy<ICdnManagementService> cdnManagementService)
            {
                _cdnManagementService = cdnManagementService;
            }

            public bool Enabled { get; set; }

            protected virtual void PurgePages(IEnumerable<IMedia> media)
            {
                if (!Enabled) return;

                var guids = media.Select(p => p.Key.ToString()).ToList();
                _cdnManagementService.Value.PurgePages(new Dictionary<string, IEnumerable<string>> { [MediaAlias] = guids }, false);
            }
        }

        public class MenuRenderingHandler : INotificationHandler<MenuRenderingNotification>
        {
            private readonly Lazy<ICdnManagementService> _cdnManagementService;

            public MenuRenderingHandler(Lazy<ICdnManagementService> cdnManagementService)
            {
                _cdnManagementService = cdnManagementService;
            }

            public void Handle(MenuRenderingNotification notification)
            {
                if (notification.TreeAlias == Constants.Trees.Content) _cdnManagementService.Value.AddPurgeMenu(notification, true);
            }
        }

        public class MediaSavedHandler : BaseContentHandler, INotificationHandler<MediaSavedNotification>
        {
            public MediaSavedHandler(Lazy<ICdnManagementService> cdnManagementService) : base(cdnManagementService) { }

            public void Handle(MediaSavedNotification notification) => PurgePages(notification.SavedEntities);
        }

        public class MediaDeletingHandler : BaseContentHandler, INotificationHandler<MediaDeletingNotification>
        {
            private readonly IMediaService _mediaService;

            public MediaDeletingHandler(Lazy<ICdnManagementService> cdnManagementService, IMediaService mediaService) : base(cdnManagementService)
            {
                _mediaService = mediaService;
            }

            public void Handle(MediaDeletingNotification notification) => PurgePages(notification.DeletedEntities.Select(x => _mediaService.GetById(x.Id)));
        }
    }
}
