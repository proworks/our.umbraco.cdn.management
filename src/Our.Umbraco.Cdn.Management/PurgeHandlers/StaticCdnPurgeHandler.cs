﻿using Our.Umbraco.Cdn.Management.Services;

namespace Our.Umbraco.Cdn.Management.PurgeHandlers
{
    public class StaticCdnPurgeHandler : ICdnPurgeHandler
    {
        public const string StaticAlias = "static";

        private readonly IStaticFileService _staticFileService;

        public StaticCdnPurgeHandler(IStaticFileService staticFileService)
        {
            _staticFileService = staticFileService;
        }

        public string Alias { get; } = StaticAlias;
        public string Name { get; } = "Static Files";
        public string PickerView { get; } = "/App_Plugins/CdnManagement/pickers/generictreepicker.html";
        public object PickerConfig { get; } = new Dictionary<string, object>
        {
            ["entityType"] = "Static",
            ["section"] = "cdnmanagement",
            ["startNodeType"] = "cdnstatic",
            ["treeAlias"] = "cdnstatic"
        };
        public bool CanSetAutoPurge { get; } = false;
        public bool AutoPurge { get; }

        public IEnumerable<string> GetAllPageUrls() => _staticFileService.GetValidUrls(_staticFileService.GetFiles(allDirectories: true).Select(x => x.RelativePath));

        public IEnumerable<string> GetPageUrls(ILookup<string, string> pagesByHandler, bool includeChildren) => _staticFileService.GetValidUrls(pagesByHandler[StaticAlias]);

        public void Initialize()
        {
        }

        public void Terminate()
        {
        }

        public void StartAutoPurge()
        {
        }

        public void StopAutoPurge()
        {
        }
    }
}