﻿using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Scoping;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.PurgeHandlers
{
    public class ContentCdnPurgeHandler : ICdnPurgeHandler
    {
        public const string ContentAlias = "content";

        private readonly IUmbracoContextFactory _umbracoContextFactory;
        private readonly IScopeProvider _scopeProvider;
        private readonly MenuRenderingHandler _menuRenderingHandler;
        private readonly ContentPublishedHandler _contentPublishedHandler;
        private readonly ContentUnpublishingHandler _contentUnpublishingHandler;
        private readonly ContentDeletingHandler _contentDeletingHandler;

        public ContentCdnPurgeHandler(IUmbracoContextFactory umbracoContextFactory,
                                      IScopeProvider scopeProvider,
                                      MenuRenderingHandler menuRenderingHandler,
                                      ContentPublishedHandler contentPublishedHandler,
                                      ContentUnpublishingHandler contentUnpublishingHandler,
                                      ContentDeletingHandler contentDeletingHandler)
        {
            _umbracoContextFactory = umbracoContextFactory;
            _scopeProvider = scopeProvider;
            _menuRenderingHandler = menuRenderingHandler;
            _contentPublishedHandler = contentPublishedHandler;
            _contentUnpublishingHandler = contentUnpublishingHandler;
            _contentDeletingHandler = contentDeletingHandler;
        }

        public string Alias { get; } = ContentAlias;
        public string Name { get; } = "Content";
        public string PickerView { get; } = "contentpicker";
        public object PickerConfig { get; } = new Dictionary<string, object> { ["multiPicker"] = "1", ["maxNumber"] = 0 };
        public bool CanSetAutoPurge { get; } = true;
        public bool AutoPurge { get; private set; }

        public IEnumerable<string> GetAllPageUrls()
        {
            using (var scope = _scopeProvider.CreateScope())
            using (var ucr = _umbracoContextFactory.EnsureUmbracoContext())
            {
                try
                {
                    var uc = ucr.UmbracoContext;
                    var urls = GetUrls(uc.Content.GetAtRoot(), true);

                    return urls;
                }
                finally
                {
                    scope.Complete();
                }
            }
        }

        public IEnumerable<string> GetPageUrls(ILookup<string, string> pagesByHandler, bool includeChildren)
        {
            using (var scope = _scopeProvider.CreateScope())
            using (var ucr = _umbracoContextFactory.EnsureUmbracoContext())
            {
                try
                {
                    var uc = ucr.UmbracoContext;
                    var urls = GetUrls(pagesByHandler[ContentAlias].Select(p =>
                        UdiParser.TryParse(p, out GuidUdi udi) && udi.EntityType == Constants.UdiEntityType.Document
                        ? uc.Content.GetById(udi.Guid)
                        : (Guid.TryParse(p, out var guid) ? uc.Content.GetById(guid) : (int.TryParse(p, out var id) ? uc.Content.GetById(id) : null))
                    ), includeChildren);

                    return urls;
                }
                finally
                {
                    scope.Complete();
                }
            }
        }

        public void Initialize() { }

        public void Terminate() { }

        private IEnumerable<string> GetUrls(IEnumerable<IPublishedContent?>? contents, bool includeChildren)
        {
            if (contents == null) yield break;

            foreach (var content in contents)
            {
                if (content == null) continue;

                var url = content.Url(mode: UrlMode.Absolute);
                if (!string.IsNullOrWhiteSpace(url)) yield return url;

                if (includeChildren)
                {
                    foreach (var child in content.Descendants())
                    {
                        url = child.Url(mode: UrlMode.Absolute);
                        if (!string.IsNullOrWhiteSpace(url)) yield return url;
                    }
                }
            }
        }

        public void StartAutoPurge()
        {
            if (AutoPurge) return;

            _contentPublishedHandler.Enabled = true;
            _contentUnpublishingHandler.Enabled = true;
            _contentDeletingHandler.Enabled = true;

            AutoPurge = true;
        }

        public void StopAutoPurge()
        {
            if (!AutoPurge) return;

            _contentPublishedHandler.Enabled = false;
            _contentUnpublishingHandler.Enabled = false;
            _contentDeletingHandler.Enabled = false;

            AutoPurge = false;
        }

        public abstract class BaseContentHandler
        {
            protected readonly Lazy<ICdnManagementService> _cdnManagementService;

            public BaseContentHandler(Lazy<ICdnManagementService> cdnManagementService)
            {
                _cdnManagementService = cdnManagementService;
            }

            public bool Enabled { get; set; }

            protected virtual void PurgePages(IEnumerable<IContent> entities)
            {
                if (!Enabled) return;

                var guids = entities.Select(p => p.Key.ToString()).ToList();
                _cdnManagementService.Value.PurgePages(new Dictionary<string, IEnumerable<string>> { [ContentAlias] = guids }, false);
            }
        }

        public class MenuRenderingHandler : INotificationHandler<MenuRenderingNotification>
        {
            private readonly Lazy<ICdnManagementService> _cdnManagementService;

            public MenuRenderingHandler(Lazy<ICdnManagementService> cdnManagementService)
            {
                _cdnManagementService = cdnManagementService;
            }

            public void Handle(MenuRenderingNotification notification)
            {
                if (notification.TreeAlias == Constants.Trees.Content) _cdnManagementService.Value.AddPurgeMenu(notification, true);
            }
        }

        public class ContentPublishedHandler : BaseContentHandler, INotificationHandler<ContentPublishedNotification>
        {
            public ContentPublishedHandler(Lazy<ICdnManagementService> cdnManagementService) : base(cdnManagementService) { }

            public void Handle(ContentPublishedNotification notification) => PurgePages(notification.PublishedEntities);
        }

        public abstract class BaseUnpublishedContentHandler : BaseContentHandler
        {
            private readonly IContentService _contentService;

            public BaseUnpublishedContentHandler(Lazy<ICdnManagementService> cdnManagementService, IContentService contentService)
                : base(cdnManagementService)
            {
                _contentService = contentService;
            }

            protected override void PurgePages(IEnumerable<IContent> entities) => base.PurgePages(entities.Select(x => _contentService.GetById(x.Id)));
        }

        public class ContentUnpublishingHandler : BaseUnpublishedContentHandler, INotificationHandler<ContentUnpublishingNotification>
        {
            public ContentUnpublishingHandler(Lazy<ICdnManagementService> cdnManagementService, IContentService contentService) : base(cdnManagementService, contentService) { }

            public void Handle(ContentUnpublishingNotification notification) => PurgePages(notification.UnpublishedEntities);
        }

        public class ContentDeletingHandler : BaseUnpublishedContentHandler, INotificationHandler<ContentDeletingNotification>
        {
            public ContentDeletingHandler(Lazy<ICdnManagementService> cdnManagementService, IContentService contentService) : base(cdnManagementService, contentService) { }

            public void Handle(ContentDeletingNotification notification) => PurgePages(notification.DeletedEntities);
        }
    }
}
