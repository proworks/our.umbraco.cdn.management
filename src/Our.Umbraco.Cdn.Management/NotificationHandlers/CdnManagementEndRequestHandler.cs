﻿using Microsoft.AspNetCore.Http;
using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Notifications;

namespace Our.Umbraco.Cdn.Management.NotificationHandlers
{
    public class CdnManagementEndRequestHandler : INotificationAsyncHandler<UmbracoRequestEndNotification>
    {
        private readonly ICdnManagementService _cdnManagementService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CdnManagementEndRequestHandler(ICdnManagementService cdnManagementService, IHttpContextAccessor httpContextAccessor)
        {
            _cdnManagementService = cdnManagementService;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task HandleAsync(UmbracoRequestEndNotification notification, CancellationToken cancellationToken)
            => _cdnManagementService.PurgeBatchedUrls(_cdnManagementService.GetBatch(_httpContextAccessor.HttpContext));
    }
}
