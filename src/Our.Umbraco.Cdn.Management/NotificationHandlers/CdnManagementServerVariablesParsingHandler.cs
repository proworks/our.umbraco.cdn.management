﻿using Microsoft.AspNetCore.Routing;
using Our.Umbraco.Cdn.Management.Controllers;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.NotificationHandlers
{
    public class CdnManagementServerVariablesParsingHandler : INotificationHandler<ServerVariablesParsingNotification>
    {
        private readonly LinkGenerator _linkGenerator;

        public CdnManagementServerVariablesParsingHandler(LinkGenerator linkGenerator)
        {
            _linkGenerator = linkGenerator;
        }

        public void Handle(ServerVariablesParsingNotification notification)
        {
            if (!notification.ServerVariables.TryGetValue("CdnManagement", out var value) || value is not Dictionary<string, object> map) notification.ServerVariables["CdnManagement"] = map = new Dictionary<string, object>();

            map["serviceUrl"] = _linkGenerator.GetUmbracoApiServiceBaseUrl<CdnManagementController>(x => x.GetAccountConfiguration());
        }
    }
}
