﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Our.Umbraco.Cdn.Management.Components;
using Our.Umbraco.Cdn.Management.Controllers;
using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.PurgeHandlers;
using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Web.BackOffice.Authorization;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management
{
    public static class ServiceCollectionExtensions
    {
        public static IUmbracoBuilder ConfigureCdnManagement(this IUmbracoBuilder builder, string writableConfigFile = "appsettings.json")
            => builder
                .AddCdnOptions(writableConfigFile)
                .AddCdnServices()
                .AddCdnHandlers()
                .AddCdnAuthorization();

        private static IUmbracoBuilder AddCdnOptions(this IUmbracoBuilder builder, string writableConfigFile = "appsettings.json")
        {
            builder.Services.ConfigureWritable<CdnConfig>(builder.Config.GetSection("CdnManagement"), writableConfigFile);

            return builder;
        }

        private static IUmbracoBuilder AddCdnServices(this IUmbracoBuilder builder)
        {
            builder.Components().Append<CdnManagementComponent>();
            builder.Services.AddUnique<IStaticFileService, StaticFileService>();
            builder.Services.AddUnique<ICdnConfiguration, CdnConfiguration>();
            builder.Services.AddUnique(x => (ICdnConfigurationWriter)x.CreateInstance<ICdnConfiguration>());
            builder.Services.AddUnique<ICdnManagementService, CdnManagementService>();

            return builder;
        }

        private static IUmbracoBuilder AddCdnHandlers(this IUmbracoBuilder builder)
            => builder
            .AddCdnHandler<ContentCdnPurgeHandler>()
            .AddAndRegisterNotificationHandler<MenuRenderingNotification, ContentCdnPurgeHandler.MenuRenderingHandler>()
            .AddAndRegisterNotificationHandler<ContentPublishedNotification, ContentCdnPurgeHandler.ContentPublishedHandler>()
            .AddAndRegisterNotificationHandler<ContentUnpublishingNotification, ContentCdnPurgeHandler.ContentUnpublishingHandler>()
            .AddAndRegisterNotificationHandler<ContentDeletingNotification, ContentCdnPurgeHandler.ContentDeletingHandler>()
            .AddCdnHandler<MediaCdnPurgeHandler>()
            .AddAndRegisterNotificationHandler<MenuRenderingNotification, MediaCdnPurgeHandler.MenuRenderingHandler>()
            .AddAndRegisterNotificationHandler<MediaSavedNotification, MediaCdnPurgeHandler.MediaSavedHandler>()
            .AddAndRegisterNotificationHandler<MediaDeletingNotification, MediaCdnPurgeHandler.MediaDeletingHandler>()
            .AddCdnHandler<StaticCdnPurgeHandler>();

        public static IUmbracoBuilder AddCdnHandler<T>(this IUmbracoBuilder builder) where T : class, ICdnPurgeHandler
        {
            builder.Services.AddUnique<T>();
            builder.Services.AddSingleton<ICdnPurgeHandler, T>();

            return builder;
        }

        public static IUmbracoBuilder AddAndRegisterNotificationHandler<TNotification, THandler>(this IUmbracoBuilder builder) where TNotification : INotification where THandler : class, INotificationHandler<TNotification>
        {
            builder.Services.AddUnique<THandler>();
            builder.AddNotificationHandler<TNotification, THandler>();

            return builder;
        }

        private static IUmbracoBuilder AddCdnAuthorization(this IUmbracoBuilder builder)
        {
            builder.Services.AddAuthorization(options => {
                options.AddPolicy(ConfigurationTreeController.ConfigurationPolicy, policy =>
                {
                    policy.AuthenticationSchemes.Add(Constants.Security.BackOfficeAuthenticationType);
                    policy.Requirements.Add(new TreeRequirement(ConfigurationTreeController.ConfigurationAlias));
                });
                options.AddPolicy(PurgingTreeController.PurgingPolicy, policy =>
                {
                    policy.AuthenticationSchemes.Add(Constants.Security.BackOfficeAuthenticationType);
                    policy.Requirements.Add(new SectionRequirement(PurgingTreeController.PurgingAlias));
                });
            });

            return builder;
        }

        public static void ConfigureWritable<T>(
             this IServiceCollection services,
             IConfigurationSection section,
             string file = "appsettings.json") where T : class, new()
        {
            services.Configure<T>(section);
            services.AddTransient<IWritableOptions<T>>(provider =>
            {
                if (provider.GetRequiredService<IConfiguration>() is not IConfigurationRoot configuration) throw new InvalidOperationException("The IConfiguration service is not an IConfigurationRoot instance");

                var environment = provider.GetRequiredService<IWebHostEnvironment>();
                var options = provider.GetRequiredService<IOptionsMonitor<T>>();

                return new WritableOptions<T>(environment, options, configuration, section.Key, file);
            });
        }
    }
}
