﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Trees;
using Umbraco.Cms.Web.BackOffice.Trees;
using Umbraco.Cms.Web.Common.Attributes;
using Umbraco.Cms.Web.Common.ModelBinders;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.Controllers
{
    public abstract class CdnTreeController : TreeController, IDiscoverable
    {
        public const string CdnManagementAlias = "cdnmanagement";
        private static readonly string RootString = Constants.System.Root.ToInvariantString();
        private readonly IMenuItemCollectionFactory _menuItemCollectionFactory;

        protected CdnTreeController(ILocalizedTextService localizedTextService,
                                    UmbracoApiControllerTypeCollection umbracoApiControllerTypeCollection,
                                    IEventAggregator eventAggregator,
                                    IMenuItemCollectionFactory menuItemCollectionFactory)
            : base(localizedTextService, umbracoApiControllerTypeCollection, eventAggregator)
        {
            _menuItemCollectionFactory = menuItemCollectionFactory;
        }

        protected abstract string RootNodeAlias { get; }
        protected abstract IEnumerable<NodeData> Nodes { get; }

        protected override ActionResult<MenuItemCollection> GetMenuForNode(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormCollection queryStrings) => _menuItemCollectionFactory.Create();

        protected override ActionResult<TreeNodeCollection> GetTreeNodes(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormCollection queryStrings)
        {
            if (id != RootString) throw new NotSupportedException();

            var nodes = new TreeNodeCollection();

            nodes.AddRange(Nodes.Select(n => CreateTreeNode(
                n.Id,
                RootString,
                queryStrings,
                n.Title,
                n.Icon ?? "icon-article",
                false,
                n.Path ?? $"{SectionAlias}/{TreeAlias}/{n.Id}"
                )));

            return nodes;
        }

        protected class NodeData
        {
            public NodeData(string id, string title)
            {
                Id = id;
                Title = title;
            }

            public string Id { get; }
            public string Title { get; }
            public string? Icon { get; set; }
            public string? Path { get; set; }
        }
    }

    [Authorize(Policy = ConfigurationPolicy)]
    [PluginController(CdnManagementAlias)]
    [Tree(CdnManagementAlias, ConfigurationAlias, TreeTitle = ConfigurationName, SortOrder = 10)]
    public class ConfigurationTreeController : CdnTreeController
    {
        public const string ConfigurationPolicy = nameof(ConfigurationPolicy);
        public const string ConfigurationAlias = "configuration";
        public const string ConfigurationName = "Configuration";

        public ConfigurationTreeController(ILocalizedTextService localizedTextService, UmbracoApiControllerTypeCollection umbracoApiControllerTypeCollection, IEventAggregator eventAggregator, IMenuItemCollectionFactory menuItemCollectionFactory)
            : base(localizedTextService, umbracoApiControllerTypeCollection, eventAggregator, menuItemCollectionFactory)
        {
        }

        protected override string RootNodeAlias { get; } = ConfigurationAlias;
        protected override IEnumerable<NodeData> Nodes { get; } = new[]
        {
            new NodeData("account", "Account"),
            new NodeData("autopurge", "Auto-Purge"),
            new NodeData("permissions", "Permissions")
        };
    }

    [Authorize(Policy = PurgingPolicy)]
    [PluginController(CdnManagementAlias)]
    [Tree(CdnManagementAlias, PurgingAlias, TreeTitle = PurgingName, SortOrder = 20)]
    public class PurgingTreeController : CdnTreeController
    {
        public const string PurgingPolicy = nameof(PurgingPolicy);
        public const string PurgingAlias = "purging";
        public const string PurgingName = "Purging";

        public PurgingTreeController(ILocalizedTextService localizedTextService, UmbracoApiControllerTypeCollection umbracoApiControllerTypeCollection, IEventAggregator eventAggregator, IMenuItemCollectionFactory menuItemCollectionFactory)
            : base(localizedTextService, umbracoApiControllerTypeCollection, eventAggregator, menuItemCollectionFactory)
        {
        }

        protected override string RootNodeAlias { get; } = PurgingAlias;
        protected override IEnumerable<NodeData> Nodes { get; } = new[]
        {
            new NodeData("purgeSite", "Entire Site"),
            new NodeData("purgePage", "Pages"),
            new NodeData("purgeUrl", "URLs")
        };
    }
}
