﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Logging;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Trees;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.BackOffice.Trees;
using Umbraco.Cms.Web.Common.Attributes;
using Umbraco.Cms.Web.Common.ModelBinders;

namespace Our.Umbraco.Cdn.Management.Controllers
{
    [Authorize(Policy = PurgingTreeController.PurgingPolicy)]
    [PluginController(CdnTreeController.CdnManagementAlias)]
    [Tree(CdnTreeController.CdnManagementAlias, "cdnstatic", TreeTitle = "Static Files", SortOrder = 10)]
    public class CdnStaticTreeController : TreeController, IDiscoverable
    {
        private readonly IStaticFileService _staticFileService;
        private readonly IMenuItemCollectionFactory _menuItemCollectionFactory;

        public CdnStaticTreeController(ILocalizedTextService localizedTextService,
                                       UmbracoApiControllerTypeCollection umbracoApiControllerTypeCollection,
                                       IEventAggregator eventAggregator,
                                       IStaticFileService staticFileService,
                                       IMenuItemCollectionFactory menuItemCollectionFactory)
            : base(localizedTextService, umbracoApiControllerTypeCollection, eventAggregator)
        {
            _staticFileService = staticFileService;
            _menuItemCollectionFactory = menuItemCollectionFactory;
        }

        protected override ActionResult<MenuItemCollection> GetMenuForNode(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormCollection queryStrings) => _menuItemCollectionFactory.Create();

        protected override ActionResult<TreeNode> CreateRootNode(FormCollection queryStrings)
        {
            // Do not display this in the main tree list, only display it in a dialog picker
            if (queryStrings != null && TreeUse.Dialog.ToString().Equals(queryStrings["use"], StringComparison.InvariantCultureIgnoreCase)) return base.CreateRootNode(queryStrings);
            
            var node = CreateTreeNode(Constants.System.RootString, null, null, "Static");

            node.HasChildren = false;
            node.ChildNodesUrl = null;
            node.MenuUrl = null;
            node.Path = null;
            node.CssClasses.Add("hidden-root-node");

            return node;
        }

        protected override ActionResult<TreeNodeCollection> GetTreeNodes(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormCollection queryStrings)
        {
            var rootPath = id != Constants.System.RootString ? $"{id}/" : "";
            var files = _staticFileService.GetFiles(rootPath, true);

            var nodes = new TreeNodeCollection();

            nodes.AddRange(files.Select(n => CreateTreeNode(
                $"{rootPath}{n.RelativePath}",
                rootPath == "" ? Constants.System.RootString : rootPath,
                queryStrings,
                n.Info.Name,
                n.Info is DirectoryInfo ? "icon-folder-close" : "icon-document",
                n.Info is DirectoryInfo
                )));

            return nodes;
        }
    }
}
