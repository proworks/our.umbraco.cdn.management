﻿using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.PurgeHandlers;
using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core.Cache;
using Umbraco.Cms.Core.Logging;
using Umbraco.Cms.Core.Models.Membership;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Web.BackOffice.Controllers;
using Umbraco.Cms.Core.Web;
using Microsoft.AspNetCore.Mvc;
using Umbraco.Cms.Core.Mapping;
using Umbraco.Cms.Core.Models.ContentEditing;
using Umbraco.Cms.Core;
using Umbraco.Extensions;
using Microsoft.Extensions.Logging;

namespace Our.Umbraco.Cdn.Management.Controllers
{
    public class CdnManagementController : UmbracoAuthorizedJsonController
    {
        private readonly ICdnConfiguration _cdnConfiguration;
        private readonly ICdnConfigurationWriter _cdnConfigurationWriter;
        private readonly ICdnManagementService _cdnManagementService;
        private readonly ICdnApiService _cdnApiService;
        private readonly IUserService _userService;
        private readonly IUmbracoMapper _umbracoMapper;
        private readonly IPublishedContentQuery _publishedContentQuery;
        private readonly ILogger<CdnManagementController> _logger;
        private readonly List<ICdnPurgeHandler> _cdnPurgeHandlers;

        public CdnManagementController(ICdnConfiguration cdnConfiguration,
                                       ICdnConfigurationWriter cdnConfigurationWriter,
                                       ICdnManagementService cdnManagementService,
                                       IEnumerable<ICdnPurgeHandler> cdnPurgeHandlers,
                                       ICdnApiService cdnApiService,
                                       IUserService userService,
                                       IUmbracoMapper umbracoMapper,
                                       IPublishedContentQuery publishedContentQuery,
                                       ILogger<CdnManagementController> logger)
        {
            _cdnConfiguration = cdnConfiguration;
            _cdnConfigurationWriter = cdnConfigurationWriter;
            _cdnManagementService = cdnManagementService;
            _cdnApiService = cdnApiService;
            _userService = userService;
            _umbracoMapper = umbracoMapper;
            _publishedContentQuery = publishedContentQuery;
            _logger = logger;
            _cdnPurgeHandlers = cdnPurgeHandlers.Distinct().ToList();
        }

        [HttpGet]
        public AccountConfiguration GetAccountConfiguration()
            => new()
            {
                ServiceAlias = _cdnApiService.ServiceAlias,
                ServiceName = _cdnApiService.ServiceName,
                ServiceOveriew = _cdnApiService.ServiceOverview,
                PropertyDefinitions = _cdnApiService.Properties,
                PropertyValues = _cdnConfiguration.GetApiConfig().ToDictionary(p => p.Key, p => (string?)p.Value)
            };

        [HttpPost]
        public Task<CdnResponse> SetAccountConfiguration(AccountConfiguration configuration)
            => TrySet(() => _cdnConfigurationWriter.UpdateAccount(configuration));

        [HttpGet]
        public AutoPurgeConfiguration GetAutoPurge()
            => new()
            {
                Handlers = _cdnPurgeHandlers.Where(x => x.CanSetAutoPurge).Select(x => new HandlerConfig { Alias = x.Alias, Name = x.Name, Enabled = x.AutoPurge }).OrderBy(x => x.Name).ToArray()
            };

        [HttpPost]
        public Task<CdnResponse> SetAutoPurge(AutoPurgeConfiguration configuration)
            => TrySet(async () => {
                foreach (var handler in configuration.Handlers ?? Array.Empty<HandlerConfig?>())
                {
                    if (handler?.Alias == null) continue;

                    await _cdnConfigurationWriter.UpdateAutoPurge(handler.Alias, handler.Enabled);
                }
            });

        [HttpGet]
        public AllowedMenuPurge GetAllowedMenuPurgeGroups()
            => new()
            {
                Groups = _umbracoMapper.MapEnumerable<IUserGroup, UserGroupBasic>(_userService.GetUserGroupsByAlias(_cdnConfiguration.AllowedMenuPurgeGroups.ToArray())).ToList()
            };

        [HttpPost]
        public Task<CdnResponse> SetAllowedMenuPurgeGroups(AllowedMenuPurge allowed)
            => TrySet(() => _cdnConfigurationWriter.UpdateAllowedMenuPurgeGroups((allowed.Groups ?? Enumerable.Empty<UserGroupBasic?>()).OfType<UserGroupBasic>().Select(i => i.Alias)));

        [HttpPost]
        public Task<CdnResponse> PurgeSite()
            => _cdnManagementService.PurgeSite();

        [HttpPost]
        public Task<CdnResponse> PurgeUrls(IEnumerable<string> urls)
            => _cdnManagementService.PurgeUrls(urls);

        [HttpPost]
        public Task<CdnResponse> PurgePages(PurgePagesRequest request)
            => _cdnManagementService.PurgePages(
                (request.PagesByHandler ?? Enumerable.Empty<PurgePageHandlerRequest?>())
                .OfType<PurgePageHandlerRequest>()
                .Where(x => x.HandlerAlias != null)
                .ToDictionary(x => x.HandlerAlias ?? string.Empty, x => (x.Pages ?? Enumerable.Empty<string?>()).OfType<string>())
                , request.IncludeChildren);

        [HttpGet]
        public Task<CdnResponse> ValidateApiService()
        {
            var siteUrl = _publishedContentQuery.ContentAtRoot()
                .Select(x => x.IsPublished()
                    && x.TemplateId.HasValue
                    && Uri.TryCreate(x.Url(mode: UrlMode.Absolute), UriKind.Absolute, out var u)
                    ? u
                    : null)
                .FirstOrDefault(x => x != null);

            return siteUrl != null
                ? _cdnApiService.ValidateConnection(siteUrl)
                : Task.FromResult(new CdnResponse { Error = "Could not find a published root node with a template and a valid URL" });
        }

        [HttpGet]
        public Task<LastCdnResponse?> GetLastResponse() => _cdnManagementService.GetLastResponse();

        [HttpGet]
        public IEnumerable<PurgeHandlerConfiguration> GetPagePickersByHandler() => _cdnPurgeHandlers.Where(x => !string.IsNullOrWhiteSpace(x.PickerView)).Select(x => new PurgeHandlerConfiguration { Alias = x.Alias, Label = x.Name, View = x.PickerView, Config = x.PickerConfig }).OrderBy(x => x.Label);

        private async Task<CdnResponse> TrySet(Func<Task> action)
        {
            try
            {
                await action();
                return "";
            }
            catch (ArgumentNullException ex)
            {
                _logger.LogError(ex, "Unable to update configuration, missing argument " + ex.ParamName);
                return $"{ex.ParamName} is required";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to update configuration");
                return "Unable to update configuration";
            }
        }
    }
}
