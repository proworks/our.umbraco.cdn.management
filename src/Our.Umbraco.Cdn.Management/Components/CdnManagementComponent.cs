﻿using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.PurgeHandlers;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.DependencyInjection;

namespace Our.Umbraco.Cdn.Management.Components
{
    public class CdnManagementComponent : IComponent
    {
        private readonly ICdnConfiguration _cdnConfiguration;
        private readonly List<ICdnPurgeHandler> _cdnPurgeHandlers;

        public CdnManagementComponent(ICdnConfiguration cdnConfiguration, IEnumerable<ICdnPurgeHandler> cdnPurgeHandlers)
        {
            _cdnConfiguration = cdnConfiguration;
            _cdnPurgeHandlers = cdnPurgeHandlers.Distinct().ToList();
        }

        public void Initialize()
        {
            _cdnPurgeHandlers.ForEach(x => x.Initialize());
            _cdnPurgeHandlers.ForEach(x => { if (_cdnConfiguration.ShouldAutoPurge(x.Alias)) { x.StartAutoPurge(); } else { x.StopAutoPurge(); } });
        }

        public void Terminate()
        {
            _cdnPurgeHandlers.ForEach(x => x.Terminate());
        }
    }
}
