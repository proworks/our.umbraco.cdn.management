﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.PurgeHandlers;
using System.Security;
using Umbraco.Cms.Core.Mapping;
using Umbraco.Cms.Core.Models.Membership;
using Umbraco.Cms.Core.Models.Trees;
using Umbraco.Cms.Core.Notifications;
using Umbraco.Cms.Core.Security;

namespace Our.Umbraco.Cdn.Management.Services
{
    public interface ICdnManagementService
    {
        void AddPurgeMenu(MenuRenderingNotification notification, bool includeChildren);
        Task<CdnResponse> PurgePages(IReadOnlyDictionary<string, IEnumerable<string>> pagesByHandler, bool includeChildren);
        Task<CdnResponse> PurgeSite();
        Task<CdnResponse> PurgeUrls(IEnumerable<string> urls);
        object? GetBatch(HttpContext? context);
        Task PurgeBatchedUrls(object? batch);
        Task<LastCdnResponse?> GetLastResponse();
    }

    public class CdnManagementService : ICdnManagementService
    {
        private const string BatchKey = nameof(CdnManagementService) + ":Batch";
        private static LastCdnResponse? _lastResponse;
        private readonly ILogger<CdnManagementService> _logger;
        private readonly ICdnApiService _cdnApiService;
        private readonly ICdnConfiguration _cdnConfiguration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IBackOfficeSecurityAccessor _backOfficeSecurityAccessor;
        private readonly IUmbracoMapper _umbracoMapper;
        private readonly List<ICdnPurgeHandler> _handlers;

        public CdnManagementService(ILogger<CdnManagementService> logger, ICdnApiService cdnApiService, IEnumerable<ICdnPurgeHandler> handlers, ICdnConfiguration cdnConfiguration, IHttpContextAccessor httpContextAccessor, IBackOfficeSecurityAccessor backOfficeSecurityAccessor, IUmbracoMapper umbracoMapper)
        {
            _logger = logger;
            _cdnApiService = cdnApiService;
            _cdnConfiguration = cdnConfiguration;
            _httpContextAccessor = httpContextAccessor;
            _backOfficeSecurityAccessor = backOfficeSecurityAccessor;
            _umbracoMapper = umbracoMapper;
            _handlers = handlers?.Distinct().ToList() ?? new List<ICdnPurgeHandler>();
        }

        public void AddPurgeMenu(MenuRenderingNotification notification, bool includeChildren)
        {
            if (notification.NodeId == "-20" || notification.NodeId == "-21" || !_cdnConfiguration.ShouldShowPurgeMenu()) return;

            var menu = new MenuItem("purgeCdn", "Purge CDN") { Icon = "icon-cdn", OpensDialog = true };
            menu.AdditionalData["jsAction"] = "cdnManagementDialogManager.purge" + (includeChildren ? "WithChildren" : "");
            notification.Menu.Items.Add(menu);
        }

        public Task<LastCdnResponse?> GetLastResponse() => Task.FromResult(_lastResponse);

        public object? GetBatch(HttpContext? context)
        {
            try
            {
                return context?.Items[BatchKey] as BatchData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Could not get batch data");
                return null;
            }
        }

        public async Task PurgeBatchedUrls(object? batchData)
        {
            try
            {
                if (batchData is not BatchData batch) return;

                if (batch.PurgeSite) SaveLastResponse(batch, await _cdnApiService.PurgeSite().ConfigureAwait(false));
                else if (batch.Urls != null && batch.Urls.Count > 0) SaveLastResponse(batch, await _cdnApiService.PurgeUrls(batch.Urls).ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Could not purge batched URLs");
            }
        }

        public async Task<CdnResponse> PurgePages(IReadOnlyDictionary<string, IEnumerable<string>> pagesByHandler, bool includeChildren)
        {
            if (pagesByHandler == null) return "";
            var pagesList = pagesByHandler.SelectMany(x => x.Value?.OfType<string>().Select(y => (x.Key, y)) ?? Enumerable.Empty<(string, string)>());
            if (!pagesList.Any()) return "";

            var urls = new List<string>();
            var pagesLookup = pagesList.ToLookup(x => x.Key, x => x.y);

            foreach (var handler in _handlers)
            {
                urls.AddRange(handler.GetPageUrls(pagesLookup, includeChildren) ?? Enumerable.Empty<string>());
            }

            return await PurgeUrls(urls, false);
        }

        public async Task<CdnResponse> PurgeSite()
        {
            if (_cdnApiService.CanPurgeSite)
            {
                return await PurgeUrls(null, true);
            }

            var urls = new List<string>();

            foreach (var handler in _handlers)
            {
                urls.AddRange(handler.GetAllPageUrls());
            }

            return await PurgeUrls(urls, true);
        }

        public Task<CdnResponse> PurgeUrls(IEnumerable<string> urls) => PurgeUrls(urls, false);

        private async Task<CdnResponse> PurgeUrls(IEnumerable<string>? urls, bool entireSite)
        {
            if (urls == null && !entireSite) return "";

            var user = _backOfficeSecurityAccessor.BackOfficeSecurity?.CurrentUser;

            if (user?.Id == null) throw new SecurityException("You must be authenticated in the Umbraco back-office to use this method");

            var batch = new BatchData(user, urls, entireSite);
            _logger.LogInformation("User {name} (ID #{id}) is purging {resource}", user.Name, user.Id, batch.Resource);

            var hc = _httpContextAccessor.HttpContext;
            if (hc == null)
            {
                if (entireSite && urls == null) return SaveLastResponse(batch, await _cdnApiService.PurgeSite());
                if (!entireSite && urls != null && urls.Any()) return SaveLastResponse(batch, await _cdnApiService.PurgeUrls(urls));
                return SaveLastResponse(batch, new CdnResponse { Message = "No URLs to purge" });
            }

            if (hc.Items[BatchKey] is not BatchData savedBatch) hc.Items[BatchKey] = new BatchData(user, urls, entireSite);
            else if (entireSite && urls == null) { savedBatch.PurgeSite = true; savedBatch.Urls.Clear(); }
            else if (!savedBatch.PurgeSite && urls != null) savedBatch.Urls.UnionWith(urls);

            return new CdnResponse { Message = "Queued to purge " + (entireSite ? "the entire site" : string.Join("; ", urls ?? Enumerable.Empty<string>())) };
        }

        private CdnResponse SaveLastResponse(BatchData batch, CdnResponse response)
        {
            try
            {
                _lastResponse = new LastCdnResponse
                {
                    Resource = batch.Resource,
                    DateTime = DateTimeOffset.UtcNow.ToString("u"),
                    Response = response,
                    User = batch.User
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Could not save the last response");
            }

            return response;
        }

        private class BatchData
        {
            public BatchData(IUser currentUser, IEnumerable<string?>? urls, bool purgeSite)
            {
                Urls = new HashSet<string>(urls?.OfType<string>() ?? new string[0]);
                PurgeSite = purgeSite;
                User = new SimpleUser
                {
                    Email = currentUser?.Email,
                    Id = currentUser?.Id ?? 0,
                    Key = currentUser?.Key ?? Guid.Empty,
                    Name = currentUser?.Name
                };
            }

            public string Resource => PurgeSite ? "the entire site" : string.Join("; ", Urls.OrderBy(x => x));
            public bool PurgeSite { get; set; }
            public HashSet<string> Urls { get; }
            public SimpleUser User { get; }
        }
    }
}
