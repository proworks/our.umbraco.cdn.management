﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System.Text.RegularExpressions;
using Umbraco.Cms.Core;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Web;

namespace Our.Umbraco.Cdn.Management.Services
{
    public interface IStaticFileService
    {
        IEnumerable<(string RelativePath, FileSystemInfo Info)> GetFiles(string? rootPath = null, bool includeDirectories = false, bool allDirectories = false);
        IEnumerable<string> GetValidUrls(IEnumerable<string> paths);
    }

    public class StaticFileService : IStaticFileService
    {
        private static readonly Regex ReservedPaths = new Regex("/((app_(browsers|data|code|globalresources|localresources|plugins|themes|webreferences)|bin|config|media|node_modules|obj|umbraco|views).*|.*\\.(config|dll|pdb|lic|exe|vue|(cs|vb)(|proj)))$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUmbracoContextAccessor _umbracoContextAccessor;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public StaticFileService(IHttpContextAccessor httpContextAccessor,
                                 IUmbracoContextAccessor umbracoContextAccessor,
                                 IWebHostEnvironment webHostEnvironment)
        {
            _httpContextAccessor = httpContextAccessor;
            _umbracoContextAccessor = umbracoContextAccessor;
            _webHostEnvironment = webHostEnvironment;
        }

        public IEnumerable<(string RelativePath, FileSystemInfo Info)> GetFiles(string? rootPath = null, bool includeDirectories = false, bool allDirectories = false)
        {
            var basePath = _webHostEnvironment.WebRootPath.TrimEnd('/', '\\');

            rootPath = rootPath?.TrimStart('/');
            if (!string.IsNullOrWhiteSpace(rootPath))
            {
                var newBase = Path.GetFullPath(Path.Combine(basePath, rootPath));
                if (newBase.StartsWith(basePath + Path.DirectorySeparatorChar)) basePath = newBase;
            }

            var dir = new DirectoryInfo(basePath);
            if (!dir.Exists) return Enumerable.Empty<(string, FileSystemInfo)>();

            var allChildItems = dir.EnumerateFileSystemInfos("*", allDirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            if (!includeDirectories) allChildItems = allChildItems.Where(x => x is FileInfo);

            var validItems = allChildItems.Select(x => (RelativePath: ValidPath(x.FullName.Substring(basePath.Length)), x)).Where(x => x.RelativePath != null).OfType<(string, FileSystemInfo)>();

            return validItems;
        }

        public IEnumerable<string> GetValidUrls(IEnumerable<string> paths)
        {
            if (!_umbracoContextAccessor.TryGetUmbracoContext(out var context)
                || _httpContextAccessor.HttpContext?.Request.GetEncodedUrl() is not string url
                || !Uri.TryCreate(url, UriKind.Absolute, out var current)) yield break;

            var domains = context.Domains.GetAll(false).Select(x => GetDomainUri(x, current)).ToList();
            if (domains.Count == 0) domains.Add(current);

            foreach (var domain in domains)
            {
                foreach (var path in GetValidPaths(paths))
                {
                    yield return new Uri(domain, path).ToString();
                }
            }
        }

        private IEnumerable<string> GetValidPaths(IEnumerable<string> paths) => paths.Select(ValidPath).OfType<string>();

        private string? ValidPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) return null;

            var relativePath = path.Replace('\\', '/');
            if (relativePath[0] != '/') relativePath = '/' + relativePath;
            if (ReservedPaths.IsMatch(relativePath)) return null;

            return relativePath;
        }

        private Uri GetDomainUri(Domain domain, Uri current)
        {
            var name = domain.Name.StartsWith("/") && current != null
                ? current.GetLeftPart(UriPartial.Authority) + domain.Name
                : domain.Name;
            var scheme = current?.Scheme ?? Uri.UriSchemeHttp;
            return new Uri(UriUtilityCore.TrimPathEndSlash(UriUtilityCore.StartWithScheme(name, scheme)));
        }
    }
}
