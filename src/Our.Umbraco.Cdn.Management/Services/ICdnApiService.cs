﻿using Our.Umbraco.Cdn.Management.Models;

namespace Our.Umbraco.Cdn.Management.Services
{
    public interface ICdnApiService
    {
        string ServiceAlias { get; }
        string ServiceName { get; }
        string ServiceOverview { get; }
        IEnumerable<CdnApiPropertyDefinition> Properties { get; }
        bool CanPurgeSite { get; }
        Task<CdnResponse> PurgeSite();
        Task<CdnResponse> PurgeUrls(IEnumerable<string?>? urls);
        Task<CdnResponse> ValidateConnection(Uri siteUrl);
    }
}
