﻿/**
 * @ngdoc
 * @name CdnManagement Service
 * @requires $http
 * 
 * @description provides the link to the CDN Management api elements
 *              required for the dashboard to function
 */

(function () {
    'use strict';

    function CdnManagementService($http) {
        var serviceRoot = Umbraco.Sys.ServerVariables.CdnManagement.serviceUrl;

        var service = {
            getAccountConfiguration,
            setAccountConfiguration,

            getAutoPurge,
            setAutoPurge,

            getAllowedMenuPurgeGroups,
            setAllowedMenuPurgeGroups,

            purgeSite,
            purgeUrls,

            getPagePickersByHandler,
            purgePages,

            validateApiService,
            getLastResponse
        };

        return service;

        /////////////////////

        function getAccountConfiguration() {
            return $http.get(serviceRoot + 'GetAccountConfiguration');
        }

        function setAccountConfiguration(serviceAlias, propertyValues) {
            return $http.post(serviceRoot + 'SetAccountConfiguration', { serviceAlias, propertyValues });
        }

        function getAutoPurge() {
            return $http.get(serviceRoot + 'GetAutoPurge');
        }

        function setAutoPurge(handlers) {
            return $http.post(serviceRoot + 'SetAutoPurge', { handlers });
        }

        function getAllowedMenuPurgeGroups() {
            return $http.get(serviceRoot + 'GetAllowedMenuPurgeGroups');
        }

        function setAllowedMenuPurgeGroups(groups) {
            return $http.post(serviceRoot + 'SetAllowedMenuPurgeGroups', { groups });
        }

        function purgeSite() {
            return $http.post(serviceRoot + 'PurgeSite');
        }

        function purgeUrls(urls) {
            return $http.post(serviceRoot + 'PurgeUrls', urls);
        }

        function getPagePickersByHandler() {
            return $http.get(serviceRoot + 'GetPagePickersByHandler');
        }

        function purgePages(pagesByHandler, includeChildren) {
            return $http.post(serviceRoot + 'PurgePages', { pagesByHandler, includeChildren });
        }

        function validateApiService() {
            return $http.get(serviceRoot + 'ValidateApiService');
        }

        function getLastResponse() {
            return $http.get(serviceRoot + 'GetLastResponse');
        }
    }

    angular.module('umbraco.services')
        .factory('CdnManagement.Service', CdnManagementService);
})();