﻿(function () {
    'use strict';

    function dialogManager($timeout, editorService, navigationService) {

        return {
            purge,
            purgeWithChildren
        };

        function purge(options, cb) {
            openPurgeDialog(options, cb, false);
        }

        function purgeWithChildren(options, cb) {
            openPurgeDialog(options, cb, true);
        }

        function openPurgeDialog(options, cb, includeChildren) {
            editorService.open({
                entity: options.entity,
                includeChildren: includeChildren,
                title: 'Purge',
                size: 'small',
                view: '/App_Plugins/CdnManagement/dialogs/purge.html',
                submit: function (done) {
                    editorService.close();
                    navigationService.hideNavigation();
                    if (cb !== undefined) {
                        cb(true);
                    }
                },
                close: function () {
                    editorService.close();
                    navigationService.hideNavigation();
                    if (cb !== undefined) {
                        cb(false);
                    }
                }
            });

            // wrap in a timeout, get rid of the 'bounce' 
            $timeout(function () {
                navigationService.hideDialog();
            });
        }
    }

    angular.module('umbraco')
        .factory('cdnManagementDialogManager', ['$timeout', 'editorService', 'navigationService', dialogManager]);
})();