﻿(function () {
    'use strict';

    function controller($scope, notificationsService, cdnManagementService) {
        var vm = this;

        vm.title = $scope.model.title;
        vm.canIncludeChildren = $scope.model.includeChildren;
        vm.entity = $scope.model.entity;
        vm.purging = false;

        vm.purge = function (includeChildren) {
            var pagesByHandler = [{ handlerAlias: vm.entity.metaData.application, pages: [vm.entity.id] }];

            vm.purging = true;

            cdnManagementService.purgePages(pagesByHandler, includeChildren).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to purge");
                } else if (result.data.error) {
                    notificationsService.error("Unable to purge: " + result.data.error);
                } else {
                    notificationsService.success("Successfully purged");
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to purge: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            }).finally(function () { vm.purging = false; vm.close(); });
        }

        vm.close = function () {
            if ($scope.model.close) $scope.model.close();
        }
    }

    angular.module('umbraco')
        .controller('CdnManagement.Purge.Controller', ['$scope', 'notificationsService', 'CdnManagement.Service', controller]);
})();