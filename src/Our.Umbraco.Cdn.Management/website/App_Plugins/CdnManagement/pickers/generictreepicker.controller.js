﻿(function () {
    'use strict';

    function genericTreePickerController($scope, editorService) {
        var vm = this;

        vm.pickedNodes = [];

        const config = angular.extend({
            entityType: "Document",
            maxNumber: 0,
            minNumber: 0,
            multiPicker: true,
            section: "content",
            sortField: "id",
            startNodeId: -1,
            startNodeType: "content",
            treeAlias: "content"
        }, $scope.model.config);

        vm.openCurrentPicker = function () {
            var selections = [];
            editorService.open({
                close: function () {
                    editorService.close();
                },
                currentNode: null,
                dataTypeKey: null,
                entityType: config.entityType,
                filterCssClass: "not-allowed not-published",
                idType: "id",
                maxNumber: config.maxNumber,
                minNumber: config.minNumber,
                multiPicker: config.multiPicker,
                section: config.section,
                select: function (node) {
                    node.selected = !node.selected;
                    if (node.selected && selections.indexOf(node) < 0) selections.push(node);
                    else if (!node.selected && selections.indexOf(node) >= 0) selections.splice(selections.indexOf(node), 1);
                },
                showEditButton: false,
                showOpenButton: false,
                showPathOnHover: false,
                size: "small",
                startNode: {
                    query: "",
                    type: config.startNodeType,
                    id: config.startNodeId
                },
                startNodeId: config.startNodeId,
                submit: function () {
                    _.each(selections, function (node) {
                        addPickedNode(node);
                    });
                    editorService.close();
                },
                treeAlias: config.treeAlias,
                view: "views/common/infiniteeditors/treepicker/treepicker.html"
            });
        };

        vm.remove = function (index) {
            if (index < 0 || index >= vm.pickedNodes.length) return;

            vm.pickedNodes.splice(index, 1);
            updateModel();
        };

        function addPickedNode(node) {
            if (_.map(vm.pickedNodes, i => i.id).indexOf(node.id) >= 0) return;

            vm.pickedNodes.push(node);
            updateModel();
        }
        function updateModel() {
            vm.pickedNodes.sort((a, b) => a[config.sortField].toString().localeCompare(b[config.sortField].toString()));
            $scope.model.value = _.map(vm.pickedNodes, i => i.id).join();
        }
    }

    angular.module('umbraco').controller("CdnManagement.GenericTreePicker.Controller", ['$scope', 'editorService', genericTreePickerController]);
})();