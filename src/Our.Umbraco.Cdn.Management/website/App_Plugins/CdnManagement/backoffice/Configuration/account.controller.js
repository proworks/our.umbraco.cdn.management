﻿(function () {
    'use strict';

    function controller(
        notificationsService, cdnManagementService) {

        var vm = this;

        vm.serviceAlias = '';
        vm.serviceName = '';
        vm.serviceOverview = '';
        vm.properties = [];
        vm.apiValidation = {};
        vm.loading = true;

        vm.loadSettings = function () {
            vm.loading = true;

            cdnManagementService.getAccountConfiguration().then(function (result) {
                if (!result || !result.data) return notificationsService.error("Could not load the account configuration");

                const data = result.data;
                vm.serviceAlias = data.serviceAlias;
                vm.serviceName = data.serviceName;
                vm.serviceOverview = data.serviceOverview;
                vm.properties = data.propertyDefinitions.map((value) => Object.assign(value, { value: data.propertyValues[value.alias] }));
            })
            .catch(function (reason) { notificationsService.error("Could not load the account configuration - " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason))); })
            .finally(function () { vm.loading = false; });

            cdnManagementService.validateApiService().then(function (result) {
                vm.apiValidation = result.data || {};
            });
        }
        vm.saveSettings = function () {
            vm.loading = true;
            var propertyValues = {};
            $.each(vm.properties, (i, property) => propertyValues[property.alias] = property.value);

            cdnManagementService.setAccountConfiguration(vm.serviceAlias, propertyValues).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to save");
                } else if (result.data.error) {
                    notificationsService.error("Unable to save: " + result.data.error);
                } else {
                    notificationsService.success("Successfully updated account configuration");
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to save: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            }).finally(function () {
                vm.loadSettings();
            });
        }

        vm.loadSettings();
    }

    angular.module('umbraco')
        .controller('CdnManagement.Configuration.Account.Controller', ['notificationsService', 'CdnManagement.Service', controller ]);
})();