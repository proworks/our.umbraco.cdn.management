﻿(function () {
    'use strict';

    function controller(
        notificationsService, cdnManagementService) {

        var vm = this;

        vm.handlers = [];
        vm.loading = true;

        vm.loadAutoPurge = function () {
            vm.loading = true;

            cdnManagementService.getAutoPurge().then(function (result) {
                if (!result || !result.data) return notificationsService.error("Could not load the auto-purge configuration");

                const data = result.data;
                vm.handlers = data.handlers;
            })
            .catch(function (reason) { notificationsService.error("Could not load the auto-purge configuration - " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason))); })
            .finally(function () { vm.loading = false; });
        }
        vm.saveAutoPurge = function (handler) {
            vm.loading = true;

            handler.enabled = !handler.enabled;
            cdnManagementService.setAutoPurge(vm.handlers).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to save");
                } else if (result.data.error) {
                    notificationsService.error("Unable to save: " + result.data.error);
                } else {
                    notificationsService.success("Successfully updated auto-purge for " + handler.name);
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to save: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            }).finally(function () {
                vm.loadAutoPurge();
            });
        }

        vm.loadAutoPurge();
    }

    angular.module('umbraco')
        .controller('CdnManagement.Configuration.AutoPurge.Controller', ['notificationsService', 'CdnManagement.Service', controller]);
})();