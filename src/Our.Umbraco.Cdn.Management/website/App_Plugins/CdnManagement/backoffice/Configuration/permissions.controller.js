﻿(function () {
    'use strict';

    function controller(
        notificationsService, editorService, cdnManagementService) {

        var vm = this;

        vm.groups = [];
        vm.loading = true;

        vm.loadGroups = function () {
            vm.loading = true;

            cdnManagementService.getAllowedMenuPurgeGroups().then(function (result) {
                if (!result || !result.data) return notificationsService.error("Could not load the permissions configuration");

                const data = result.data;
                vm.groups = data.groups || [];
            })
                .catch(function (reason) { notificationsService.error("Could not load the permissions configuration - " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason))); })
                .finally(function () { vm.loading = false; });
        };

        vm.saveGroups = function () {
            vm.loading = true;

            cdnManagementService.setAllowedMenuPurgeGroups(vm.groups).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to save");
                } else if (result.data.error) {
                    notificationsService.error("Unable to save: " + result.data.error);
                } else {
                    notificationsService.success("Successfully updated group permissions");
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to save: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            }).finally(function () {
                vm.loadGroups();
            });
        };

        vm.removeSelectedItem = function (idx, list) {
            if (!list || list.length <= 0 || idx >= list.length) return;

            list.splice(idx, 1);
            vm.saveGroups();
        };

        vm.openUserGroupPicker = function () {
            var currentSelection = [];
            for (var i = 0; i < vm.groups.length; i++) currentSelection.push(vm.groups[i]);

            var userGroupPicker = {
                selection: currentSelection,
                submit: function (model) {
                    // apply changes
                    if (model.selection) {
                        vm.groups = model.selection;
                        vm.saveGroups();
                    }
                    editorService.close();
                },
                close: function () {
                    // rollback on close
                    editorService.close();
                }
            };
            editorService.userGroupPicker(userGroupPicker);
        };

        vm.loadGroups();
    }

    angular.module('umbraco')
        .controller('CdnManagement.Configuration.Permissions.Controller', ['notificationsService', 'editorService', 'CdnManagement.Service', controller]);
})();