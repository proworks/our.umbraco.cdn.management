﻿(function () {
    'use strict';

    function controller(
        notificationsService, $timeout, cdnManagementService) {

        var vm = this;

        vm.addUrl = "";
        vm.urls = [];
        vm.purging = false;
        vm.lastResponse = {};

        vm.add = function () {
            if (vm.addUrl && vm.addUrl.length) vm.urls.push(vm.addUrl);
            vm.addUrl = "";
        }
        vm.remove = function (idx) {
            if (idx >= 0 && idx < vm.urls.length) vm.urls.splice(idx, 1);
        }
        vm.purge = function () {
            if (vm.urls.length === 0) { notificationsService.error("You must select at least one page to purge"); return; }

            vm.loading = true;

            cdnManagementService.purgeUrls(vm.urls).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to purge");
                } else if (result.data.error) {
                    notificationsService.error("Unable to purge: " + result.data.error);
                } else {
                    notificationsService.success(result.data.message && result.data.message.length ? result.data.message : "Successfully purged");
                    vm.getLastResponse(true);
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to purge: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            });
        }

        vm.getLastResponse = function (skipTimeout) {
            var promise = cdnManagementService.getLastResponse().then(function (result) {
                vm.lastResponse = result.data || {};
                if (vm.lastResponse.dateTime) {
                    const dt = new Date(vm.lastResponse.dateTime);
                    if (!isNaN(dt.getTime())) vm.lastResponse.dateTime = dt.toLocaleString();
                }
            });

            if (!skipTimeout) promise.finally(function () { $timeout(vm.getLastResponse, 2000); });
        }

        vm.getLastResponse();
    }

    angular.module('umbraco')
        .controller('CdnManagement.Purging.Url.Controller', ['notificationsService', '$timeout', 'CdnManagement.Service', controller]);
})();