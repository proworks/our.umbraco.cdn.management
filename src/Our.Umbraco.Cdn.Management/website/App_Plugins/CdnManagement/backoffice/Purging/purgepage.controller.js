﻿(function () {
    'use strict';

    function controller(
        notificationsService, $timeout, cdnManagementService) {

        var vm = this;

        vm.purgeConfigs = [];
        vm.purging = false;
        vm.lastResponse = {};

        vm.purge = function (includeChildren) {
            var pagesByHandler = [];
            var pageCount = 0;
            for (var i = 0; i < vm.purgeConfigs.length; i++) {
                const purgeConfig = vm.purgeConfigs[i];

                if (!purgeConfig.value || !purgeConfig.value.length) continue;

                const pages = purgeConfig.value.split(/,/g);
                pageCount += pages.length;
                pagesByHandler.push({ handlerAlias: purgeConfig.alias, pages });
            }

            if (pageCount === 0) { notificationsService.error("You must select at least one page to purge"); return; }

            vm.purging = true;

            cdnManagementService.purgePages(pagesByHandler, includeChildren).then(function (result) {
                if (!result || !result.data) {
                    notificationsService.error("Unable to purge");
                } else if (result.data.error) {
                    notificationsService.error("Unable to purge: " + result.data.error);
                } else {
                    notificationsService.success(result.data.message && result.data.message.length ? result.data.message : "Successfully purged");
                    vm.getLastResponse(true);
                }
            }).catch(function (reason) {
                notificationsService.error("Failed to purge: " + (reason && reason.data && reason.data.Message ? reason.data.Message : (reason && reason.statusText ? reason.statusText : reason)));
            }).finally(function () { vm.purging = false; });
        }

        cdnManagementService.getPagePickersByHandler().then(function (result) {
            if (!result || !result.data || !result.data.length) return;

            vm.purgeConfigs = result.data;
        });

        vm.getLastResponse = function (skipTimeout) {
            var promise = cdnManagementService.getLastResponse().then(function (result) {
                vm.lastResponse = result.data || {};
                if (vm.lastResponse.dateTime) {
                    const dt = new Date(vm.lastResponse.dateTime);
                    if (!isNaN(dt.getTime())) vm.lastResponse.dateTime = dt.toLocaleString();
                }
            });

            if (!skipTimeout) promise.finally(function () { $timeout(vm.getLastResponse, 2000); });
            
        }

        vm.getLastResponse();
    }

    angular.module('umbraco')
        .controller('CdnManagement.Purging.Page.Controller', ['notificationsService', '$timeout', 'CdnManagement.Service', controller]);
})();