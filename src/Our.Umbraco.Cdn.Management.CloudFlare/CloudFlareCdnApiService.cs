﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Our.Umbraco.Cdn.Management.Models;
using Our.Umbraco.Cdn.Management.Services;
using System.Collections.Concurrent;
using System.Net.Http.Headers;
using System.Text;

namespace Our.Umbraco.Cdn.Management.CloudFlare
{
    public class CloudFlareCdnApiService : ICdnApiService
    {
        private static readonly ConcurrentDictionary<string, string> _zoneIdentifiersByHostName = new ConcurrentDictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        private readonly ILogger<CloudFlareCdnApiService> _logger;
        private readonly ICloudFlareCdnConfig _config;

        public CloudFlareCdnApiService(ILogger<CloudFlareCdnApiService> logger, ICloudFlareCdnConfig config)
        {
            _logger = logger;
            _config = config;
        }

        public bool CanPurgeSite { get; } = true;
        public string ServiceAlias { get; } = CloudFlareCdnConfig.ServiceAlias;
        public string ServiceName { get; } = "CloudFlare";
        public IEnumerable<CdnApiPropertyDefinition> Properties { get; } = new[]
        {
            new CdnApiPropertyDefinition(CloudFlareCdnConfig.KeyAlias, "API Key/Token", "The API with ability to purge cache for the given zone"),
            new CdnApiPropertyDefinition(CloudFlareCdnConfig.EmailAlias, "Email", "The email associated with the CloudFlare account.  This is required if an API Key is used, but is not required when an API Token is used", false),
            new CdnApiPropertyDefinition(CloudFlareCdnConfig.ZoneIdentifierAlias, "Zone Identifier", "The zone identifier for the zone containing cached records to manage.  If not provided, the hostname of the content URL will be used to lookup the appropriate zone identifier, and must match the zone name exactly", false),
            new CdnApiPropertyDefinition(CloudFlareCdnConfig.CloudFlareApiBaseUrlAlias, "API Url", $"An optional URL to use when sending purge requests.  Mainly for testing, if not specified, the calls will be made to the CloudFlare default of {CloudFlareCdnConfig.DefaultApiUrl}", false)
        };
        public string ServiceOverview { get; } = "<p>Follow the instructions at <a href='https://api.cloudflare.com/' target='_blank'>Cloudflare's API Documentation</a> to generate a new API Key or API Token.  API Keys grant access to all resources and require both a key and an email to work.  API Tokens can be restricted, and do not require providing an email.  For the API Token, you will need to grant it the Purge permission on the Zone:CachePurge resource.  If you don't specify a zone identifier, you will also need to grant it the Read permission on the Zone:Zone resource.</p>";

        public async Task<CdnResponse> PurgeSite() => await Purge((await GetZoneIdentifiersByHostName().ConfigureAwait(false)).Values.Select(x => new PurgeEverythingRequest { ZoneIdentifier = x })).ConfigureAwait(false);
        public async Task<CdnResponse> PurgeUrls(IEnumerable<string?>? urls) => await Purge((await GetUrlsByZoneIdentifier(urls).ConfigureAwait(false)).Select(x => new PurgeUrlsRequest { ZoneIdentifier = x.Key, Files = x })).ConfigureAwait(false);
        public Task<CdnResponse> ValidateConnection(Uri siteUrl) => PurgeUrls(new[] { new Uri(siteUrl, "robots.txt").ToString() });

        private async Task<CdnResponse> Purge(IEnumerable<IPurgeRequest> contents)
        {
            CdnResponse response = "";

            foreach (var content in contents)
            {
                response = await Purge(content).ConfigureAwait(false);
                if (response.Error != null) return response;
            }

            return response;
        }

        private async Task<CdnResponse> Purge(IPurgeRequest content)
        {
            if (!ValidateConfig(out var resp)) return resp;

            using var client = new HttpClient();
            using var request = new HttpRequestMessage(HttpMethod.Post, new Uri(_config.CloudFlareApiBaseUrl, $"zones/{_config.ZoneIdentifier}/purge_cache"));
            using var httpContent = CreateHttpContent(content);
            if (_config.Email != null)
            {
                request.Headers.Add("X-Auth-Key", _config.Key);
                request.Headers.Add("X-Auth-Email", _config.Email);
            }
            else
            {
                request.Headers.Add("X-Auth-User-Service-Key", _config.Key);
            }
            request.Content = httpContent;

            using var response = await client.SendAsync(request).ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("CloudFlare returned a status code of {code}. Request={request}, Response={response}", response.StatusCode, JsonConvert.SerializeObject(content), await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                return $"CloudFlare returned a response code of {response.StatusCode}";
            }

            using var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
            using var rdr = new StreamReader(stream, new UTF8Encoding(false));
            using var jtr = new JsonTextReader(rdr);
            var result = new JsonSerializer().Deserialize<PurgeResponse>(jtr);

            if (result != null && result.Success) return "";

            _logger.LogError("CloudFlare could not process purge request.  Request={request}, Response={response}", JsonConvert.SerializeObject(content), JsonConvert.SerializeObject(result));
            return "CloudFlare returned one or more errors: " + string.Join("; ", (result?.Errors ?? Enumerable.Empty<Error>()).Select(e => $"{e.Code} - {e.Message}"));
        }

        private async Task<Dictionary<string, string>> GetZoneIdentifiersByHostName()
        {
            if (_config.ZoneIdentifier != null) return new Dictionary<string, string> { [""] = _config.ZoneIdentifier };
            if (_zoneIdentifiersByHostName.Count > 0) return new Dictionary<string, string>(_zoneIdentifiersByHostName, StringComparer.InvariantCultureIgnoreCase);
            if (!ValidateConfig(out _)) return new Dictionary<string, string>();

            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(_config.CloudFlareApiBaseUrl, $"zones")))
            {
                if (_config.Email != null)
                {
                    request.Headers.Add("X-Auth-Key", _config.Key);
                    request.Headers.Add("X-Auth-Email", _config.Email);
                }
                else
                {
                    request.Headers.Add("X-Auth-User-Service-Key", _config.Key);
                }

                using (var response = await client.SendAsync(request).ConfigureAwait(false))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.LogError("CloudFlare returned a status code of {code}. Response={response}", response.StatusCode, await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                        return new Dictionary<string, string>();
                    }

                    using (var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    using (var rdr = new StreamReader(stream, new UTF8Encoding(false)))
                    using (var jtr = new JsonTextReader(rdr))
                    {
                        var result = new JsonSerializer().Deserialize<ZoneResponse>(jtr);
                        if (result is null || !result.Success) { _logger.LogError("CloudFlare could not process zone request.  Response={response}", result != null ? JsonConvert.SerializeObject(result) : "null"); return new Dictionary<string, string>(); } 

                        if (result?.Result is not null)
                        {
                            foreach (var item in result.Result)
                            {
                                if (item?.Name is null || item.Id is null) continue;

                                _zoneIdentifiersByHostName[item.Name] = item.Id;
                            }
                        }
                    }
                }
            }

            return new Dictionary<string, string>(_zoneIdentifiersByHostName, StringComparer.InvariantCultureIgnoreCase);
        }

        private async Task<ILookup<string?, string>> GetUrlsByZoneIdentifier(IEnumerable<string?>? nullableUrls)
        {
            var urls = (nullableUrls ?? Enumerable.Empty<string?>()).OfType<string>();
            var zoneIdentifiersByHostName = await GetZoneIdentifiersByHostName().ConfigureAwait(false);
            var defaultId = zoneIdentifiersByHostName.Keys.FirstOrDefault();

            if (zoneIdentifiersByHostName.Count == 1) return urls.ToLookup(x => defaultId);
            return urls.ToLookup(x => zoneIdentifiersByHostName.TryGetValue(new Uri(x).Host, out var d) ? d : defaultId);
        }

        private HttpContent CreateHttpContent(object content)
        {
            var ms = new MemoryStream();
            SerializeJsonIntoStream(content, ms);
            ms.Seek(0, SeekOrigin.Begin);

            var httpContent = new StreamContent(ms);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return httpContent;
        }

        private void SerializeJsonIntoStream(object content, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, content);
                jtw.Flush();
            }
        }

        private bool ValidateConfig(out CdnResponse response)
        {
            var errors = new List<string>();

            if (_config.Key == null) errors.Add("Missing the API Key.");

            response = errors.Count == 0 ? "" : string.Join("  ", errors);
            return errors.Count == 0;
        }

        private interface IPurgeRequest
        {
            string? ZoneIdentifier { get; }
        }

        private class PurgeEverythingRequest : IPurgeRequest
        {
            [JsonProperty("purge_everything")]
            public bool PurgeEverything { get; set; } = true;

            [JsonIgnore]
            public string? ZoneIdentifier { get; set; }
        }

        private class PurgeUrlsRequest : IPurgeRequest
        {
            [JsonProperty("files")]
            public IEnumerable<string>? Files { get; set; }

            [JsonIgnore]
            public string? ZoneIdentifier { get; set; }
        }

        private class PurgeResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("errors")]
            public IEnumerable<Error>? Errors { get; set; }

            [JsonProperty("messages")]
            public IEnumerable<object>? Messages { get; set; }

            [JsonProperty("result")]
            public PurgeResult? Result { get; set; }
        }

        private class PurgeResult
        {
            [JsonProperty("id")]
            public string? Id { get; set; }
            [JsonProperty("name")]
            public string? Name { get; set; }
        }

        private class ZoneResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("errors")]
            public IEnumerable<Error>? Errors { get; set; }

            [JsonProperty("messages")]
            public IEnumerable<object>? Messages { get; set; }

            [JsonProperty("result")]
            public ZoneResult[]? Result { get; set; }
        }

        private class ZoneResult
        {
            [JsonProperty("id")]
            public string? Id { get; set; }
            [JsonProperty("name")]
            public string? Name { get; set; }
        }

        private class Error
        {
            [JsonProperty("code")]
            public int Code { get; set; }

            [JsonProperty("message")]
            public string? Message { get; set; }
        }
    }
}
