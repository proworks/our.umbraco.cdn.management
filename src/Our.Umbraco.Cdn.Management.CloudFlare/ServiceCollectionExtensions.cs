﻿using Our.Umbraco.Cdn.Management.Services;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Extensions;

namespace Our.Umbraco.Cdn.Management.CloudFlare
{
    public static class ServiceCollectionExtensions
    {
        public static IUmbracoBuilder ConfigureCloudFlareCdnManagement(this IUmbracoBuilder builder, string writableConfigFile = "appsettings.json")
            => builder
                .ConfigureCdnManagement(writableConfigFile)
                .AddServices();

        private static IUmbracoBuilder AddServices(this IUmbracoBuilder builder)
        {
            builder.Services.AddUnique<ICdnApiService, CloudFlareCdnApiService>();
            builder.Services.AddUnique<ICloudFlareCdnConfig, CloudFlareCdnConfig>();

            return builder;
        }
    }
}
