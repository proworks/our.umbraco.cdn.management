﻿using Our.Umbraco.Cdn.Management.Models;

namespace Our.Umbraco.Cdn.Management.CloudFlare
{
    public interface ICloudFlareCdnConfig
    {
        Uri CloudFlareApiBaseUrl { get; }
        string? ZoneIdentifier { get; }
        string? Email { get; }
        string? Key { get; }
    }

    public class CloudFlareCdnConfig : ICloudFlareCdnConfig
    {
        public const string ServiceAlias = "CloudFlare";
        public const string CloudFlareApiBaseUrlAlias = "cloudFlareBaseUrl";
        public const string ZoneIdentifierAlias = "zoneIdentifier";
        public const string EmailAlias = "email";
        public const string KeyAlias = "key";

        internal const string DefaultApiUrl = "https://api.cloudflare.com/client/v4/";
        private readonly ICdnConfiguration _cdnConfiguration;

        public CloudFlareCdnConfig(ICdnConfiguration cdnConfiguration)
        {
            _cdnConfiguration = cdnConfiguration;
        }

        public Uri CloudFlareApiBaseUrl => Uri.TryCreate(_cdnConfiguration.GetApiProperty(CloudFlareApiBaseUrlAlias, ServiceAlias), UriKind.Absolute, out var uri) && uri is not null ? uri : new Uri(DefaultApiUrl);
        public string? ZoneIdentifier => _cdnConfiguration.GetApiProperty(ZoneIdentifierAlias, ServiceAlias);
        public string? Email => _cdnConfiguration.GetApiProperty(EmailAlias, ServiceAlias);
        public string? Key => _cdnConfiguration.GetApiProperty(KeyAlias, ServiceAlias);
    }
}
